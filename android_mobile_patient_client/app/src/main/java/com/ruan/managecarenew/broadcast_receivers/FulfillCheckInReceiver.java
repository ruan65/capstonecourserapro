package com.ruan.managecarenew.broadcast_receivers;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Vibrator;
import android.util.Log;

import com.ruan.managecarenew.activities.CheckInActivity;
import com.ruan.managecarenew.activities.MainActivity;

public class FulfillCheckInReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        Log.d("ml", " inside onReceive, intent: " + intent.getExtras());

        Intent startIntent = new Intent(context.getApplicationContext(), MainActivity.class);

        startIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        context.startActivity(startIntent);

        Vibrator vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);

        vibrator.vibrate(1000);

    }
}
