package com.ruan.managecarenew.activities;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.widget.Toast;

import com.ruan.managecarenew.R;
import com.ruan.managecarenew.broadcast_receivers.FulfillCheckInReceiver;

import java.util.Calendar;
import java.util.Date;

public class SettingsActivity extends PreferenceActivity {

    SharedPreferences prefs;
    AlarmManager alarmManager;
    Intent intent;
    PendingIntent pendingIntent;
    Context ctx;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);

        ctx = getApplicationContext();
        alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        getPreferenceManager()
                .findPreference(getString(R.string.pref_key_enable_alerts))
                .setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {

                    @Override
                    public boolean onPreferenceChange(Preference preference, Object checked) {
                        if ((Boolean) checked) {
                            startAlerts();
                        } else {
                            stopAlerts();
                        }
                        return true;
                    }
                });

        getPreferenceManager().findPreference(getString(R.string.alerts))
                .setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                    @Override
                    public boolean onPreferenceClick(Preference preference) {

                        String text = prefs.getString(getString(R.string.alerts), "");
                        Toast.makeText(ctx, text, Toast.LENGTH_LONG).show();
                        return true;
                    }
                });
    }

    @Override
    protected void onResume() {
        super.onResume();

        String action = getIntent().getAction();
        if (action != null && action.equals(getString(R.string.pref_key_enable_alerts))) {
            startAlerts();
            finish();
        }
    }

    public void startAlerts() {

        int first = prefs.getInt("check_in_first", 600); // minutes
        int last = prefs.getInt("check_in_last", 1320);

        int checkins_per_day = Integer.parseInt(prefs.getString("checkins_per_day", "4"));

        int interval = (last - first) / checkins_per_day;

        intent = new Intent(this, FulfillCheckInReceiver.class);

        alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.set(Calendar.HOUR_OF_DAY, first / 60);
        calendar.set(Calendar.MINUTE, first % 60);

        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < checkins_per_day; i++) {

            long triggerAtMillis = calendar.getTimeInMillis() + i * interval * 60 * 1000;

            String time = "Check-In time: " + new Date(triggerAtMillis);
            builder.append(time + "\n");
            Log.d("ml", time);

            pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), i, intent, 0);

            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP,
                    triggerAtMillis, AlarmManager.INTERVAL_DAY, pendingIntent);
        }

        prefs.edit().putString(getString(R.string.alerts), builder.toString()).commit();
    }

    public void stopAlerts() {

        if (alarmManager != null) {

            int checkins_per_day = Integer.parseInt(
                    prefs.getString("checkins_per_day", "24"));

            intent = new Intent(this, FulfillCheckInReceiver.class);

            for (int i = 0; i < checkins_per_day; i++) {

                pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), i, intent, 0);

                alarmManager.cancel(pendingIntent);
            }
        }
        finish();
    }
}
