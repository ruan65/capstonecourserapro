package com.ruan.managecare;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan
@EnableAutoConfiguration
public class Application { //implements CommandLineRunner {


//    @Autowired
//    CheckInsRepository repository;


    public static void main(String[] args) {


        SpringApplication.run(Application.class, args);

        System.out.println("Good Spring Booting, Andrew: good luck with all this cool stuff...");


    }

//    @Override
//    public void run(String... args) throws Exception {
//        repository.save(new CheckIn());
//    }
}
