package com.ruan.managecare.clients_access;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configurers.GlobalAuthenticationConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.servlet.configuration.EnableWebMvcSecurity;

@Configuration
@EnableWebMvcSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {


    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.authorizeRequests()
//                .antMatchers("/questions").hasRole("ADMIN")
//                .antMatchers("/auth").hasRole("USER")
                .antMatchers("/", "main").permitAll().anyRequest().authenticated()
                .and()
                .httpBasic()
                .and()
                .csrf().disable();

        http.formLogin()
                .loginPage("/login").permitAll()
                .and()
                .logout().permitAll();
    }

//    @Override
//    public void configure(WebSecurity webSecurity) throws Exception {
//
//        webSecurity.ignoring()
//                .antMatchers("/resources/**");
//    }

    @Configuration
    protected static class AuthConfig extends GlobalAuthenticationConfigurerAdapter {

        @Override
        public void init(AuthenticationManagerBuilder auth) throws Exception {

            auth.inMemoryAuthentication()
                    .withUser("MR-111-5555").password("5555111").roles("USER");

            auth.inMemoryAuthentication()
                    .withUser("MR-222-6666").password("6666222").roles("USER");

            auth.inMemoryAuthentication()
                    .withUser("MR-333-7777").password("7777333").roles("USER");

            auth.inMemoryAuthentication()
                    .withUser("MR-444-8888").password("8888444").roles("USER");

            auth.inMemoryAuthentication()
                    .withUser("MR-555-9999").password("9999555").roles("USER");

            auth.inMemoryAuthentication()
                    .withUser("MR-000-4444").password("4444000").roles("USER");

            auth.inMemoryAuthentication()
                    .withUser("DL-007123").password("house").roles("USER");

            auth.inMemoryAuthentication()
                    .withUser("DL-117123").password("little").roles("USER");
        }
    }
}
